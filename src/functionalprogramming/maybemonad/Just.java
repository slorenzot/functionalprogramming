/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.maybemonad;

/**
 *
 * @author Soulberto Lorenzo
 */
public class Just<T> extends Maybe<T> {

    public <R> Maybe<R> join() {
        return Maybe.of(this);
    }
    
}
