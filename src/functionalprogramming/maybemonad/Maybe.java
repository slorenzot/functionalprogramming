/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.maybemonad;

import functionalprogramming.bindmonad.Functor;
import java.util.function.Function;

/**
 *
 * @author Soulberto Lorenzo
 */
public class Maybe<T> implements Functor<T> {

    public static final Maybe<?> NOTHING = Maybe.of(new Maybe() {

        @Override
        public String toString() {
            return "";
        }

    });

    private final T val;

    public final static <T> Maybe of(T v) {
        if (v == null) {
            return new Maybe(Maybe.NOTHING.apply());
        }
        return new Maybe<>(v);
    }

    public Maybe() {
        this.val = null;
    }

    public Maybe(T val) {
        this.val = val;
    }

    @Override
    public Functor<T> pure(T v) {
        return new Maybe(v);
    }

    @Override
    public <F> Functor<F> map(Function<T, F> f) {
        if (val == null) {
            return new Maybe(Maybe.NOTHING);
        }
        return new Maybe<>(f.apply(val));
    }

    @Override
    public <F> Functor<F> flatMap(Function<T, Functor<F>> f) {
        return f.apply(val);
    }

    @Override
    public T apply() {
        return this.val;
    }

}
