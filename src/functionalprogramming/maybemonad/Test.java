/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.maybemonad;

/**
 *
 * @author Soulberto Lorenzo
 */
public class Test {

    public static void main(String[] args) {
        Maybe val1 = Maybe.NOTHING;
        Maybe<String> val2 = Maybe.of("Cadena de Texto");
        Maybe val3 = Maybe.of(null);

        System.out.println(val1
                .map(c -> "Hello World!")
                .apply());
        String txt = val2
                .map(String::toUpperCase)
                .apply();
        System.out.println(txt);
        
        System.out.println(val3.equals(Maybe.NOTHING));
        System.out.println(val1.equals(Maybe.NOTHING));

        Maybe<String> val = Maybe.of("");
        System.out.println(val
                .map(t -> t.concat("x"))
                .apply());
        System.out.println(val
                .apply());
    }
}
