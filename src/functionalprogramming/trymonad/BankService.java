/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.trymonad;

import functionalprogramming.trymonad.transactional.Failure;
import functionalprogramming.trymonad.transactional.Reader;
import functionalprogramming.trymonad.transactional.Success;
import functionalprogramming.trymonad.transactional.TryReader;
import java.math.BigDecimal;

/**
 *
 * @author Soulberto
 */
public class BankService {

    private static final BigDecimal INITIAL_BALANCE = BigDecimal.ZERO;

    public static TryReader<BankConnection, Account> open(String owner, String number, BigDecimal balance) {
        return TryReader.of(c -> {
            if (INITIAL_BALANCE.compareTo(BigDecimal.ZERO) < 0) {
                return new Failure(new InsufficientBalanceError("Initial Balance is negative!"));
            }
            
            return new Success(new Account(owner, number, new Balance(balance)));
        });
    }

    public static Reader<BankConnection, Account> credit(Account account, BigDecimal value) {
        return Reader.of(c -> {
            return new Account(account.owner, account.number, new Balance(account.balance.amount.add(value)));
        });
    }

    public static TryReader<BankConnection, Account> debit(Account account, BigDecimal value) {
        return TryReader.of(c -> {
            if (account.balance.amount.compareTo(value) < 0) {
                return new Failure(new InsufficientBalanceError("Account Balance is insufficient!"));
            }

            return new Success(new Account(account.owner, account.number, new Balance(account.balance.amount.subtract(value))));
        });
    }
}
