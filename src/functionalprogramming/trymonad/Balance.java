/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.trymonad;

import java.math.BigDecimal;

/**
 *
 * @author Soulberto
 */
public class Balance {
    
    public BigDecimal amount;

    public Balance(BigDecimal amount) {
        this.amount = amount;
    }
}
