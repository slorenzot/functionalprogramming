/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.trymonad;

import functionalprogramming.trymonad.transactional.Try;
import java.math.BigDecimal;

/**
 *
 * @author Soulberto
 */
public class Test {

    public static void main(String[] args) {
        Try<Account> transaction
                = BankService.open("Alice", "123", new BigDecimal(100.00))
                        .map(acc -> BankService.credit((Account) acc, new BigDecimal(200.00)))
                        .map(acc -> BankService.credit(acc, new BigDecimal(300.00)))
//                        .flatMap(acc -> BankService.debit(acc, new BigDecimal(900.00)))
                        .apply(new BankConnection());

        System.out.println(transaction);
        
        if (transaction.isFailure()) {
            System.out.println("FAIL: " + ((Exception) transaction.apply()).getMessage());
        } else {
            System.out.println("OK: " + transaction.apply());
        }
    }
}
