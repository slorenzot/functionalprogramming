/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.trymonad.transactional;

import java.util.function.Function;

/**
 *
 * @author Soulberto
 */
public class TryReader<R, T> {

    private final Function<R, Try<T>> run;
    
    public static <U, V> TryReader<U, V> of(Function<U, Try<V>> f) {
        return new TryReader<U, V>(f);
    }

    public TryReader(Function<R, Try<T>> run) {
        this.run = run;
    }

    public <F> TryReader<R, F> map(Function<T, Reader<R, F>> f) {
        return new TryReader<R, F>((R r) -> apply(r)
                .map(e -> f.apply(e).apply(r)));
    }

    public <F> TryReader<R, F> flatMap(Function<T, TryReader<R, F>> f) {
        return new TryReader<R, F>((R r) -> apply(r)
                .flatMap(e -> f.apply(e).apply(r)));
    }

    public Try<T> apply(R r) {
        return run.apply(r);
    }

}
