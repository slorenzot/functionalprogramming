/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.trymonad.transactional;

import java.util.function.Function;

/**
 *
 * @author Soulberto
 */
public class Reader<R, T> {
    private final Function<R, T> run;
    
    public static <U, V> Reader<U, V> of(Function<U, V> f) {
        return new Reader<U, V>(f);
    }

    public Reader(Function<R, T> run) {
        this.run = run;
    }
    
    public <F> Reader<R, F> map(Function<T, F> f) {
        return Reader.of((R r) -> f.apply(apply(r)));
    }
    
    public <F> Reader<R, F> flatMap(Function<T, Reader<R, F>> f) {
        return Reader.of((R r) -> f.apply(apply(r)).apply(r));
    }
    
    public T apply(R r) {
        return run.apply(r);
    }
    
}
