/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.trymonad.transactional;

import java.util.function.Function;

/**
 *
 * @author Soulberto
 * @param <T>
 */
public interface Try<T> {
    
    Object apply();

    <F> Try<F> map(Function<T, F> f);

    <F> Try<F> flatMap(Function<T, Try<F>> f);

    boolean isFailure();
}
