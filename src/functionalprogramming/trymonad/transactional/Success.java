/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.trymonad.transactional;

import java.util.function.Function;

/**
 *
 * @author Soulberto
 */
public class Success<T> implements Try<T> {

    private final T value;

    public Success(T value) {
        this.value = value;
    }

    @Override
    public boolean isFailure() {
        return false;
    }

    @Override
    public <F> Try<F> map(Function<T, F> f) {
        return new Success<>(f.apply(value));
    }

    @Override
    public <F> Try<F> flatMap(Function<T, Try<F>> f) {
        return f.apply(value);
    }

    @Override
    public Object apply() {
        return value;
    }
}
