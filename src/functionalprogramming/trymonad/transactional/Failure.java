/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.trymonad.transactional;

import java.util.function.Function;

/**
 *
 * @author Soulberto
 */
public class Failure<T> implements Try<T> {

    private final Exception error;

    public Failure(Exception error) {
        this.error = error;
    }

    @Override
    public boolean isFailure() {
        return true;
    }

    @Override
    public <F> Try<F> map(Function<T, F> f) {
        return (Failure<F>) this;
    }

    @Override
    public <F> Try<F> flatMap(Function<T, Try<F>> f) {
        return (Failure<F>) this;
    }
    
    @Override
    public Object apply() {
        return error;
    }

}
