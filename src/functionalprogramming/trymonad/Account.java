/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.trymonad;

import functionalprogramming.trymonad.transactional.Success;
import functionalprogramming.trymonad.transactional.Try;
import functionalprogramming.trymonad.transactional.Failure;
import java.math.BigDecimal;

/**
 *
 * @author Soulberto
 */
public class Account {

    final String owner;
    final String number;
    public Balance balance = new Balance(BigDecimal.ZERO);

    public Account(String owner, String number) {
        this.owner = owner;
        this.number = number;
    }

    public Account(String owner, String number, Balance balance) {
        this.owner = owner;
        this.number = number;
        this.balance = balance;
    }

    public Try<Account> credit(BigDecimal value) {
        return new Success<>(new Account(owner, number, new Balance(balance.amount.add(value))));
    }

    public Try<Account> debit(BigDecimal value) {
        if (balance.amount.compareTo(value) < 0) {
            return new Failure<>(new InsufficientBalanceError());
        }

        return new Success<>(new Account(owner, number, new Balance(balance.amount.subtract(value))));
    }
}
