/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.bindmonad;

import java.util.function.Function;

/**
 *
 * @author Soulberto Lorenzo
 */
public class Identity<T> implements BiFunctor<T, Identity<?>> {

    private final T value;

    Identity(T value) {
        this.value = value;
    }

    public <R> Identity<R> map(Function<T, R> f) {
        final R result = f.apply(value);
        return new Identity<>(result);
    }

}
