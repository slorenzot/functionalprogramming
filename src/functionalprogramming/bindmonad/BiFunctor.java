/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.bindmonad;

import java.util.function.Function;

/**
 *
 * @author Soulberto Lorenzo
 */
interface BiFunctor<T, F extends BiFunctor<?, ?>> {

    <R> F map(Function<T, R> f);
}
