/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.bindmonad;

import java.util.function.Function;

/**
 *
 * @author Soulberto Lorenzo
 */
public interface Functor<T> {

    Functor<T> pure(T v);

    <R> Functor<R> map(Function<T, R> f);

    <R> Functor<R> flatMap(Function<T, Functor<R>> f);
    
    T apply();
    
}
