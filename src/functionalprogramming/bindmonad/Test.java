/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.bindmonad;

/**
 *
 * @author Soulberto Lorenzo
 */
public class Test {

    public static void main(String[] args) {
        Monad<Integer> number = Monad.of(2);
        
        System.out.println(
                number
                        .map(f -> f * 2)
                        .map(f -> Math.pow(f, 2))
                        .apply());
        
        Double d = number
                        .map(f -> f * 2)
                        .map(f -> Math.pow(f, 2))
                        .apply();
        
        System.out.println(d);
    }
    
}
