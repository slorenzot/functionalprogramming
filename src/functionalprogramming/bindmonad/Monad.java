/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.bindmonad;

import java.util.function.Function;

/**
 *
 * @author Soulberto Lorenzo
 */
public class Monad<T> implements Functor<T> {

    private final T val;

    public final static <T> Monad of(T v) {
        return new Monad(v);
    }

    public Monad(final T value) {
        this.val = value;
    }

    @Override
    public final Monad<T> pure(final T v) {
        return Monad.of(v);
    }

    @Override
    public <F> Monad<F> map(Function<T, F> f) {
        return Monad.of(f.apply(val));
    }

    @Override
    public <F> Monad<F> flatMap(Function<T, Functor<F>> f) {
        return (Monad<F>) f.apply(val);
    }
    
    @Override
    public T apply() {
        return val;
    }

}
