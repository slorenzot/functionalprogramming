/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.tuples;

import java.util.stream.Stream;

/**
 *
 * @author slorenzo
 */
public interface Tuple<T> {

    T get(int p);

    Object[] unpack();

    Stream<T> stream();

    @Override
    String toString();

}
