/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
// */
package functionalprogramming.tuples;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author slorenzo
 * @param <T>
 */
public class NTuple<T> implements Tuple<T> {

    public static <T> NTuple of(T... values) {
        return new NTuple(values);
    }

    private final List<Optional<T>> values;

    public NTuple(T... values) {
        super();

        this.values = Arrays.asList(values).stream()
                .map(v -> Optional.ofNullable(v))
                .collect(Collectors.toList());
    }

    public NTuple(int size, List<Integer> values) {
        super();

        this.values = values.stream()
                .limit(size)
                .map(v -> Optional.ofNullable((T) v))
                .collect(Collectors.toList());
    }

    @Override
    public Object[] unpack() {
        return values.stream()
                .map(e -> e.orElse((T) "null"))
                .collect(Collectors.toList()).toArray();
    }

    @Override
    public T get(int p) {
        return values.get(p).get();
    }

    @Override
    public Stream<T> stream() {
        return values.stream()
                .map(e -> (T) (e.orElse(null)));
    }

    @Override
    public String toString() {
        return String.format("(%s)", values.stream()
                .map(e -> e.orElse((T) null).toString())
                .collect(Collectors.joining(",")));
    }

}
