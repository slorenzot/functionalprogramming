/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.with;

import java.math.BigDecimal;

/**
 *
 * @author slorenzo
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        BigDecimal bobSalary = new SalaryCalculator()
                .with(SalaryRules::bonus)
                .with(SalaryRules::surcharge)
                .with(SalaryRules::allowance)
                .with(s -> s.multiply(BigDecimal.valueOf(2)))
                .apply(BigDecimal.valueOf(16000.06));

//
        System.out.println(bobSalary);
    }

}
