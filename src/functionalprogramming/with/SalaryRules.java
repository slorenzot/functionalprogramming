/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.with;

import java.math.BigDecimal;

/**
 *
 * @author slorenzo
 */
public final class SalaryRules {

    public static BigDecimal allowance(BigDecimal salary) {
        return salary.multiply(BigDecimal.valueOf(1.2));
    }

    public static BigDecimal bonus(BigDecimal salary) {
        return salary.multiply(BigDecimal.valueOf(1.1));
    }

    public static BigDecimal tax(BigDecimal salary) {
        return salary.multiply(BigDecimal.valueOf(0.7));
    }
    
    public static BigDecimal surcharge(BigDecimal salary) {
        return salary.multiply(BigDecimal.valueOf(0.9));
    }

}
