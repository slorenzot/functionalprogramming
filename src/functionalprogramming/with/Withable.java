/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.with;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 *
 * @author slorenzo
 * @param <T>
 */
public class Withable<T> {

    private final Function<T, T> func;
    
    protected Withable() {
        this(Function.identity());
    }

    public Withable(Function<T, T> f) {
        this.func = f;
    }
    
    public Withable<T> with(Function<T, T> f) {
        return new Withable<>(func.andThen(f));
    }

    public final T apply(T value) {
        return func.apply(value);
    }

    public final T apply(Supplier<T> supplier) {
        return func.apply(supplier.get());
    }
    
}
