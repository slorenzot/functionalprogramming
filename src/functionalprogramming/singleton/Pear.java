/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.singleton;

import javafx.scene.paint.Color;

/**
 *
 * @author Soulberto Lorenzo
 */
class Pear extends Singleton<Pear> {
    
    public Color color = Color.RED;
    
    public Pear() {
        System.out.println("A Pear!");
    }
    
}
