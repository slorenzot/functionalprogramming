/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.singleton;

import java.util.function.Supplier;

/**
 *
 * @author Soulberto Lorenzo
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Pear pear = Singleton.of(Pear.class);
        System.out.println("Eager Pear!");
        System.out.println(pear);
        System.out.println(pear);
        
        Supplier<Apple> supplier = Singleton.supplier(Apple.class);
        System.out.println("Lazy Apple!");
        System.out.println(supplier.get());
        System.out.println(supplier.get());
    }
    
}
