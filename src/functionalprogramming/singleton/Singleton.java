/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.singleton;

import java.util.HashMap;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Soulberto Lorenzo
 */
public abstract class Singleton<T> {
    /**
     * Almacena todos los objetos creados usando Singleton.
     */
    private static volatile HashMap mapping = new HashMap<String, Object>();

    /**
     * Retorna una instancia única de la clase indicada. Implementa el llamado EAGER,
     * creando el objeto en el momento en que se invoca el método of().
     *
     * @param <T>
     * @param clazz
     * @return
     */
    public final static synchronized <T> T of(final Class<T> clazz) {
        String key = clazz.getCanonicalName();

        return Optional
                .ofNullable((T) mapping.get(key))
                .orElseGet(() -> {
                    T instance = null;
                    try {
                        instance = clazz.newInstance();
                        mapping.put(key, instance);
                    } catch (InstantiationException | IllegalAccessException ex) {
                        Logger.getLogger(Singleton.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return instance;
                });
    }

    /**
     * Retorna un proveedor que devuelve una única instancia de la clase indicada.
     * Implementa el llamado LAZY o Peresozo, lo que permite obtener el objeto
     * únicamente cuando es necesario llamando el método get(). Este comportamiento
     * lo hace útil cuando se usan objetos costosos en recursos, como por eje.;
     * conexiones de bases de datos, archivos, entre otros.
     * 
     * @param <T>
     * @param clazz
     * @return 
     */
    public final static synchronized <T> Supplier<T> supplier(final Class<T> clazz) {
        return new Supplier<T>() {
            @Override
            public T get() {
                return Singleton.of(clazz);
            }
        };
    }

}
