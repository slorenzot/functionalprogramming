/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionalprogramming.eitherMonad;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 *
 * @author Soulberto Lorenzo
 */
public class Either<T, U> {

    private BiFunction<T, U, Either> f;
            
    private T left = null;
    private U right = null;

    public Either(T t, U u) {
        left = t;
        right = u;
    }

    public Either(BiFunction<T, U, Either> f) {
        this.f = f;
    }

    public static <T, U> Either<T, U> of(T t, U u) {
        return new Either(t, u);
    }

    public static <A, B> Either<A, B> left(A a) {
        return new Either<>(a, null);
    }

    public T left() {
        return left;
    }

    public boolean isLeft() {
        return right != null;
    }

    public static <A, B> Either<A, B> right(B b) {
        return new Either<>(null, b);
    }

    public U right() {
        return right;
    }

    public boolean isRight() {
        return left != null;
    }

    public Either<U, T> swap() {
        return Either.of(right, left);
    }

    public <R, S> Either<R, S> map(Function<T, R> ft, Function<U, S> fu) {
        return Either.of(ft.apply(left), fu.apply(right));
    }

}
